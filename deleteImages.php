<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT');
// para que no guarde en cache
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    echo json_encode(array('status' => false));
    exit;
}

$store = $_POST['store'];
$url = $_POST['url'];
$path = './stores/'.$url;

if ($url !== null) {

    if (file_exists($path)) {
    
        unlink($path); //elimino el fichero
        
        echo json_encode(array(
            'status' => true,
            'msg' => 'Se borró la imagen'.$path
        ));
        exit;
    }else{
            echo json_encode(array(
                'status' => false,
                'msg' => 'no existe la imagen '.$url.'y el path '.$path
            ));
    }

}else{
    echo json_encode(
        array('status' => false, 'msg' => 'No se pudo borrar la imagen '.$path)
    );
    exit;
}