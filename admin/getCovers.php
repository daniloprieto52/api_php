<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT');
// para que no guarde en cache
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    echo json_encode(array('status' => false));
    exit;
}

$section = $_POST['section'];
$type = $_POST['type'];

$path = './covers/'.$section.'/'.$type.'/';

$allFiles = glob($path.'*'); // Recupero el nombre de todos los archivos en el directorio

echo json_encode(array(
    'status' => true,
    'data'   => $allFiles
));
exit;