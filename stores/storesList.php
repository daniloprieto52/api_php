<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT');
// para que no guarde en cache
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

/*
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    echo json_encode(array('status' => false));
    exit;
}
*/



$path = scandir('./');
$array = array();
$storesUrl = array();


for ($i = 0; $i <= count($path); $i++) {
     if (is_dir($path[$i])){
         $array[] = $path[$i];
     };
}

$prueba = './'.$array[2].'/'.$array[2].'.json';
$json;



for ($i = 0; $i <= count($array); $i++){
    $url = './'.$array[$i].'/'.$array[$i].'.json';
    
    if (is_file($url)){
        $data = file_get_contents($url);
        $json = json_decode($data, true);
        $storesUrl[] = $json;
        
    }
}


echo json_encode(array(
    'status' => true,
    'data'    => $storesUrl,
));
exit;